﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    [Authorize]
    public class ACVipController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /ACVip/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.ACVips.ToList());
        }

        //
        // GET: /ACVip/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            ACVip acvip = db.ACVips.Find(id);
            if (acvip == null)
            {
                return HttpNotFound();
            }
            return View(acvip);
        }

        //
        // GET: /ACVip/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ACVip/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ACVip acvip)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                acvip.UserProfileID = userid;
                db.ACVips.Add(acvip);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(acvip);
        }

        //
        // GET: /ACVip/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ACVip acvip = db.ACVips.Find(id);
            if (acvip == null)
            {
                return HttpNotFound();
            }
            return View(acvip);
        }

        //
        // POST: /ACVip/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ACVip acvip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(acvip).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(acvip);
        }

        //
        // GET: /ACVip/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ACVip acvip = db.ACVips.Find(id);
            if (acvip == null)
            {
                return HttpNotFound();
            }
            return View(acvip);
        }

        //
        // POST: /ACVip/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ACVip acvip = db.ACVips.Find(id);
            db.ACVips.Remove(acvip);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}