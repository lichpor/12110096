﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    [Authorize]
    public class LapTrinhController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /LapTrinh/
        [AllowAnonymous]
        public ActionResult Index(string Publishers, string strSearch)
        {
            var laptrinht = db.Laptrinhs.Include(p => p.UserProfile);
            //return View(posts.ToList());
            //Select all Book records
            var laptrinhs = from b in db.Laptrinhs

                            select b;

            //Get list of Book publisher
            var publisherList = from c in laptrinhs
                                orderby c.Title
                                select c.Title;

            //Set distinct list of publishers in ViewBag property
            //ViewBag.Publishers = new SelectList(publisherList.Distinct());

            //Search records by Book Name 
            if (!string.IsNullOrEmpty(strSearch))
                laptrinhs = laptrinhs.Where(m => m.Title.Contains(strSearch));
            return View(laptrinhs);
            //return View(db.DiaDanhs.ToList());
        }

        //
        // GET: /LapTrinh/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            LapTrinh laptrinh = db.Laptrinhs.Find(id);
            if (laptrinh == null)
            {
                return HttpNotFound();
            }
            ViewData["iddietvr"] = 1;
            ViewData["idlaptrinh"] = id;
            ViewData["Loai"] = 2;
            return View(laptrinh);
        }

        //
        // GET: /LapTrinh/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LapTrinh/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LapTrinh laptrinh)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                laptrinh.UserProfileID = userid;
                laptrinh.DateCreate = DateTime.Now;
                db.Laptrinhs.Add(laptrinh);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(laptrinh);
        }

        //
        // GET: /LapTrinh/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LapTrinh laptrinh = db.Laptrinhs.Find(id);
            if (laptrinh == null)
            {
                return HttpNotFound();
            }
            return View(laptrinh);
        }

        //
        // POST: /LapTrinh/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LapTrinh laptrinh)
        {
            if (ModelState.IsValid)
            {
                laptrinh.DateCreate = DateTime.Now;
                db.Entry(laptrinh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(laptrinh);
        }

        //
        // GET: /LapTrinh/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LapTrinh laptrinh = db.Laptrinhs.Find(id);
            if (laptrinh == null)
            {
                return HttpNotFound();
            }
            return View(laptrinh);
        }

        //
        // POST: /LapTrinh/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LapTrinh laptrinh = db.Laptrinhs.Find(id);
            db.Laptrinhs.Remove(laptrinh);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}