﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    [Authorize]
    public class DietVRController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /DietVR/
        [AllowAnonymous]
        public ActionResult Index(string Publishers, string strSearch)
        {
            var dietvr = db.DietVRs.Include(p => p.UserProfile);
            //return View(posts.ToList());
            //Select all Book records
            var dietvirus = from b in db.DietVRs

                          select b;

            //Get list of Book publisher
            var publisherList = from c in dietvirus
                                orderby c.Title
                                select c.Title;

            //Set distinct list of publishers in ViewBag property
            //ViewBag.Publishers = new SelectList(publisherList.Distinct());

            //Search records by Book Name 
            if (!string.IsNullOrEmpty(strSearch))
                dietvirus = dietvirus.Where(m => m.Title.Contains(strSearch));
            return View(dietvirus);
            //return View(db.DiaDanhs.ToList());
        }

        //
        // GET: /DietVR/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            DietVR dietvr = db.DietVRs.Find(id);
            if (dietvr == null)
            {
                return HttpNotFound();
            }
            ViewData["idlaptrinh"] = 1;
            ViewData["iddietvr"] = id;
            ViewData["Loai"] = 1;
            return View(dietvr);
        }

        //
        // GET: /DietVR/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DietVR/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DietVR dietvr, string Content)
        {
            if (ModelState.IsValid)
            {
                dietvr.DateCreate = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                dietvr.UserProfileID = userid;
                //Tao list các tag
                List<Tag> Tags = new List<Tag>();
                //tach cac Tag theo dau ,
                string[] TagContent = Content.Split(',');
                //Lập các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem tag content đã có hay chưa
                    Tag TagExists = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item.Trim()));
                    if (ListTag.Count() > 0)
                    {
                        //nếu có tag rồi thì add thêm Post vào
                        TagExists = ListTag.First();
                        TagExists.DietVRs.Add(dietvr);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới
                        TagExists = new Tag();
                        TagExists.Content = item.Trim();
                        TagExists.DietVRs = new List<DietVR>();
                        TagExists.DietVRs.Add(dietvr);
                    }
                    //add vào List các tag
                    Tags.Add(TagExists);
                }
                //Gán List Tag cho Post
                dietvr.Tags = Tags;
                db.DietVRs.Add(dietvr);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dietvr);
        }

        //
        // GET: /DietVR/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DietVR dietvr = db.DietVRs.Find(id);
            if (dietvr == null)
            {
                return HttpNotFound();
            }
            return View(dietvr);
        }

        //
        // POST: /DietVR/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DietVR dietvr)
        {
            if (ModelState.IsValid)
            {
                dietvr.DateCreate = DateTime.Now;
                db.Entry(dietvr).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dietvr);
        }

        //
        // GET: /DietVR/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DietVR dietvr = db.DietVRs.Find(id);
            if (dietvr == null)
            {
                return HttpNotFound();
            }
            return View(dietvr);
        }

        //
        // POST: /DietVR/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DietVR dietvr = db.DietVRs.Find(id);
            db.DietVRs.Remove(dietvr);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}