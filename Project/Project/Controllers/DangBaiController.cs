﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    [Authorize]
    public class DangBaiController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /DangBai/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.DangBais.ToList());
        }

        //
        // GET: /DangBai/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            DangBai dangbai = db.DangBais.Find(id);
            if (dangbai == null)
            {
                return HttpNotFound();
            }
            return View(dangbai);
        }

        //
        // GET: /DangBai/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DangBai/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DangBai dangbai)
        {
            if (ModelState.IsValid)
            {
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                dangbai.UserProfileID = userid;
                db.DangBais.Add(dangbai);
                db.SaveChanges();
                return PartialView("viewDangBai", dangbai);
            }

            return View(dangbai);
        }

        //
        // GET: /DangBai/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DangBai dangbai = db.DangBais.Find(id);
            if (dangbai == null)
            {
                return HttpNotFound();
            }
            return View(dangbai);
        }

        //
        // POST: /DangBai/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DangBai dangbai)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dangbai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dangbai);
        }

        //
        // GET: /DangBai/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DangBai dangbai = db.DangBais.Find(id);
            if (dangbai == null)
            {
                return HttpNotFound();
            }
            return View(dangbai);
        }

        //
        // POST: /DangBai/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DangBai dangbai = db.DangBais.Find(id);
            db.DangBais.Remove(dangbai);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}