﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class RatingVRController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /RatingVR/

        public ActionResult Index()
        {
            var ratingvrs = db.RatingVRs.Include(r => r.DietVRs);
            return View(ratingvrs.ToList());
        }

        //
        // GET: /RatingVR/Details/5

        public ActionResult Details(int id = 0)
        {
            RatingVR ratingvr = db.RatingVRs.Find(id);
            if (ratingvr == null)
            {
                return HttpNotFound();
            }
            return View(ratingvr);
        }

        //
        // GET: /RatingVR/Create

        public ActionResult Create()
        {
            ViewBag.DietVRID = new SelectList(db.DietVRs, "ID", "Title");
            return View();
        }

        //
        // POST: /RatingVR/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RatingVR ratingvr,int iddietvr)
        {
            if (ModelState.IsValid)
            {
                ratingvr.DietVRID = iddietvr;
                db.RatingVRs.Add(ratingvr);
                db.SaveChanges();
                return RedirectToAction("Details/" + iddietvr, "DietVR");
            }

            ViewBag.DietVRID = new SelectList(db.DietVRs, "ID", "Title", ratingvr.DietVRID);
            return View(ratingvr);
        }

        //
        // GET: /RatingVR/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RatingVR ratingvr = db.RatingVRs.Find(id);
            if (ratingvr == null)
            {
                return HttpNotFound();
            }
            ViewBag.DietVRID = new SelectList(db.DietVRs, "ID", "Title", ratingvr.DietVRID);
            return View(ratingvr);
        }

        //
        // POST: /RatingVR/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RatingVR ratingvr)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ratingvr).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DietVRID = new SelectList(db.DietVRs, "ID", "Title", ratingvr.DietVRID);
            return View(ratingvr);
        }

        //
        // GET: /RatingVR/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RatingVR ratingvr = db.RatingVRs.Find(id);
            if (ratingvr == null)
            {
                return HttpNotFound();
            }
            return View(ratingvr);
        }

        //
        // POST: /RatingVR/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RatingVR ratingvr = db.RatingVRs.Find(id);
            db.RatingVRs.Remove(ratingvr);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}