﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class RatingLTController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /RatingLT/

        public ActionResult Index()
        {
            var ratinglts = db.RatingLTs.Include(r => r.LapTrinhs);
            return View(ratinglts.ToList());
        }

        //
        // GET: /RatingLT/Details/5

        public ActionResult Details(int id = 0)
        {
            RatingLT ratinglt = db.RatingLTs.Find(id);
            if (ratinglt == null)
            {
                return HttpNotFound();
            }
            return View(ratinglt);
        }

        //
        // GET: /RatingLT/Create

        public ActionResult Create()
        {
            ViewBag.LapTrinhID = new SelectList(db.Laptrinhs, "ID", "Title");
            return View();
        }

        //
        // POST: /RatingLT/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RatingLT ratinglt,int idlaptrinh)
        {
            if (ModelState.IsValid)
            {
                ratinglt.LapTrinhID = idlaptrinh;
                db.RatingLTs.Add(ratinglt);
                db.SaveChanges();
                return RedirectToAction("Details/" + idlaptrinh, "LapTrinh");
            }

            ViewBag.LapTrinhID = new SelectList(db.Laptrinhs, "ID", "Title", ratinglt.LapTrinhID);
            return View(ratinglt);
        }

        //
        // GET: /RatingLT/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RatingLT ratinglt = db.RatingLTs.Find(id);
            if (ratinglt == null)
            {
                return HttpNotFound();
            }
            ViewBag.LapTrinhID = new SelectList(db.Laptrinhs, "ID", "Title", ratinglt.LapTrinhID);
            return View(ratinglt);
        }

        //
        // POST: /RatingLT/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RatingLT ratinglt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ratinglt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LapTrinhID = new SelectList(db.Laptrinhs, "ID", "Title", ratinglt.LapTrinhID);
            return View(ratinglt);
        }

        //
        // GET: /RatingLT/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RatingLT ratinglt = db.RatingLTs.Find(id);
            if (ratinglt == null)
            {
                return HttpNotFound();
            }
            return View(ratinglt);
        }

        //
        // POST: /RatingLT/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RatingLT ratinglt = db.RatingLTs.Find(id);
            db.RatingLTs.Remove(ratinglt);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}