﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class ACVip
    {
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public int ID { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String TenAC { set; get; }

        [DataType(DataType.Date)]
        public DateTime NgayHH { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String MK { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }
    }
}