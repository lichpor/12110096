﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class RatingVR
    {
        public int ID { set; get; }
        //1 Danh gia chi danh cho 1 bai viet cua trung tam du lich 
        public int DietVRID { set; get; }
        public DietVR DietVRs { set; get; }
        //Diem danh Gia
        [Range(1, 5, ErrorMessage = "Diem danh gia tu 1 toi 5")]
        public float? Diem { set; get; }
    }
}