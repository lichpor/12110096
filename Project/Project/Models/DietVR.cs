﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class DietVR
    {
        public int ID { set; get; }
        //Tieu De
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20-500", MinimumLength = 2)]
        public String Title { set; get; }

        //Noi Dung
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(5000, ErrorMessage = "Số lượng ký tự từ 20-5000", MinimumLength = 2)]
        public String Body { set; get; }

        //Ngay dang
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }

        //1 bài viết của 1 người dùng
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }
        // 1 bài viết có nhiều Comment
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<RatingVR> RatingVRs { set; get; }
        //1 bài viết có nhiều tag
        public virtual ICollection<Tag> Tags { set; get; }
    }
}