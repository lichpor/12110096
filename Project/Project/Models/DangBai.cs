﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class DangBai
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20-500", MinimumLength = 2)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(5000, ErrorMessage = "Số lượng ký tự từ 20-5000", MinimumLength = 2)]
        public String Body { set; get; }
        public String Tagg { set; get; }
        //1 bài viết mới của 1 người dùng đăng
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }
    }
}