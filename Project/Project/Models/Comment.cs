﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Comment
    {
        public int ID { set; get; }
        //Nội dung
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String Body { set; get; }
        //Ngày đăng
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        //Tác Giả
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public String Author { set; get; }
        //Mail
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }
        public int Loai { set; get; }
        public int DietVRID { set; get; }
        public virtual DietVR DietVRs { set; get; }
        public int LapTrinhID { set; get; }
        public virtual LapTrinh LapTrinhs { set; get; }
    }
}