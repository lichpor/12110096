namespace Project.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RatingVRs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DietVRID = c.Int(nullable: false),
                        Diem = c.Single(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DietVRs", t => t.DietVRID, cascadeDelete: true)
                .Index(t => t.DietVRID);
            
            CreateTable(
                "dbo.DietVRs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.LapTrinhs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false, maxLength: 500),
                        DateCreated = c.DateTime(nullable: false),
                        Author = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Loai = c.Int(nullable: false),
                        DietVRID = c.Int(nullable: false),
                        LapTrinhID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DietVRs", t => t.DietVRID, cascadeDelete: true)
                .ForeignKey("dbo.LapTrinhs", t => t.LapTrinhID, cascadeDelete: true)
                .Index(t => t.DietVRID)
                .Index(t => t.LapTrinhID);
            
            CreateTable(
                "dbo.RatingLTs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LapTrinhID = c.Int(nullable: false),
                        Diem = c.Single(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.LapTrinhs", t => t.LapTrinhID, cascadeDelete: true)
                .Index(t => t.LapTrinhID);
            
            CreateTable(
                "dbo.DangBais",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 500),
                        Body = c.String(nullable: false),
                        Tagg = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.ACVips",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenAC = c.String(nullable: false, maxLength: 500),
                        NgayHH = c.DateTime(nullable: false),
                        MK = c.String(nullable: false, maxLength: 500),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Tag_DietVR",
                c => new
                    {
                        DietVRID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DietVRID, t.TagID })
                .ForeignKey("dbo.DietVRs", t => t.DietVRID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.DietVRID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_DietVR", new[] { "TagID" });
            DropIndex("dbo.Tag_DietVR", new[] { "DietVRID" });
            DropIndex("dbo.ACVips", new[] { "UserProfile_UserId" });
            DropIndex("dbo.DangBais", new[] { "UserProfile_UserId" });
            DropIndex("dbo.RatingLTs", new[] { "LapTrinhID" });
            DropIndex("dbo.Comments", new[] { "LapTrinhID" });
            DropIndex("dbo.Comments", new[] { "DietVRID" });
            DropIndex("dbo.LapTrinhs", new[] { "UserProfile_UserId" });
            DropIndex("dbo.DietVRs", new[] { "UserProfile_UserId" });
            DropIndex("dbo.RatingVRs", new[] { "DietVRID" });
            DropForeignKey("dbo.Tag_DietVR", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_DietVR", "DietVRID", "dbo.DietVRs");
            DropForeignKey("dbo.ACVips", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.DangBais", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.RatingLTs", "LapTrinhID", "dbo.LapTrinhs");
            DropForeignKey("dbo.Comments", "LapTrinhID", "dbo.LapTrinhs");
            DropForeignKey("dbo.Comments", "DietVRID", "dbo.DietVRs");
            DropForeignKey("dbo.LapTrinhs", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.DietVRs", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.RatingVRs", "DietVRID", "dbo.DietVRs");
            DropTable("dbo.Tag_DietVR");
            DropTable("dbo.Tags");
            DropTable("dbo.ACVips");
            DropTable("dbo.DangBais");
            DropTable("dbo.RatingLTs");
            DropTable("dbo.Comments");
            DropTable("dbo.LapTrinhs");
            DropTable("dbo.UserProfile");
            DropTable("dbo.DietVRs");
            DropTable("dbo.RatingVRs");
        }
    }
}
