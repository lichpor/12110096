﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QL_Account.Models
{
    public class Tag
    {
        public int ID { set; get; }
        //
        [Required(ErrorMessage = "Không được bỏ trống.")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự từ 10 đến 100 ký tự", MinimumLength = 1)]
        public String Content { set; get; }
        //1 Tag co nhieu Post
        public virtual ICollection<Post> Posts { set; get; }
    }
}