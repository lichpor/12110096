﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QL_Account.Models
{
    public class Comment
    {
        public int ID { set; get; }
        //
        [Required(ErrorMessage = "Không được bỏ trống.")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String Body { set; get; }
        //
        [Required(ErrorMessage = "Không được bỏ trống.")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        //
        [Required(ErrorMessage = "Không được bỏ trống.")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { set; get; }
        //
        [Required(ErrorMessage = "Không được bỏ trống.")]
        public String Author { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual Post Posts { set; get; }
    }
}