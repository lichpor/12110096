﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage="Không được bỏ trống.")]
        [StringLength(500,ErrorMessage="Số lượng kí tự nắm trong khoảng từ 20 đến 500.",MinimumLength=20)]
        public String Title { set; get; }
        [Required(ErrorMessage = "Không được bỏ trống.")]
        [StringLength(1000, ErrorMessage = "Số lượng kí tự tối thiểu là 50 kí tự.",MinimumLength=50)]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

    }
}