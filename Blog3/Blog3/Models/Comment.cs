﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống.")]
        [StringLength(1000, ErrorMessage = "Số lượng kí tự tối thiểu là 50 kí tự.", MinimumLength = 50)]
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public System.DateTime DateCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        [DataType(DataType.DateTime)]
        public System.DateTime DateUpdated { set; get; }
        public int PostID { set; get; }
        public virtual Post Posts { set; get; }
    }
}