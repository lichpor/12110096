﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Post
    {

        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(500, ErrorMessage = "Số lượng kí tự nằm trong khoảng từ 10 đến 500", MinimumLength = 10)]
        public string Title { set; get; }


        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(1000, ErrorMessage = "Số lượng ký tự tối thiểu là 50 ký tự", MinimumLength = 50)]
        public string Body { set; get; }


        [Required(ErrorMessage = "Nhập ngày hợp lệ")]
        [DataType(DataType.Date, ErrorMessage = "Nhập ngày hợp lệ!")]
        public System.DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Nhập ngày hợp lệ")]
        [DataType(DataType.Date, ErrorMessage = "Nhập ngày hợp lệ!")]
        public System.DateTime DateUpdated { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        public int AccountID { set; get; }

        public virtual Account Account { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}