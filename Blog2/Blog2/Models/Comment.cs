﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(1000, ErrorMessage = "Số lượng kí tự tối thiểu là 50 kí tự", MinimumLength = 50)]
        public string Body { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.Date, ErrorMessage = "Phải nhập đúng Ngày/Tháng/Năm")]
        public System.DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.Date, ErrorMessage = "Phải nhập đúng Ngày/Tháng/Năm")]
        public System.DateTime DateUpdated { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        public string Author { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        public int PostID { set; get; }

        public virtual Post Post { set; get; }
    }
}