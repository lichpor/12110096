﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Account
    {
        public int AccountID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.Password)]
        public string Password { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Phải nhập đúng địa chỉ Email")]
        public string Email { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(30, ErrorMessage = "Số lượng kí tự tối đa là 100 kí tự", MinimumLength = 1)]
        public string FirstName { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(30, ErrorMessage = "Số lượng kí tự tối đa là 100 kí tự", MinimumLength = 1)]
        public string LastName { set; get; }


        public virtual ICollection<Post> Posts { set; get; }
    }
}