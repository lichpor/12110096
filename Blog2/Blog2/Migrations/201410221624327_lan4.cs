namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
        }
    }
}
