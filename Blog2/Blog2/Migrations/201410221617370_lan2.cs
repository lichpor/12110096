namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
